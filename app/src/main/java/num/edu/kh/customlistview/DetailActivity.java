package num.edu.kh.customlistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    private String[] detail={"The apple isn't just a fruit. It's a symbol — from the apples eaten by Adam and Eve in the Biblical creation story, which symbolize the loss of innocent to the expression \"American as apple pie\" to mean something that is wholesome. The \"apple of your eye\" is the person who delights you — maybe you are the apple of your grandfather's eye. He might tell you, \"An apple a day keeps the doctor away,\" meaning the fruit is good for your health.",
                              "A banana is a curved, yellow fruit with a thick skin and soft sweet flesh. If you eat a banana every day for breakfast, your roommate might nickname you \"the monkey",
                            "Oranges grow in warm places like Florida, California, and Brazil, where they are harvested to be eaten and made into juice and other products. The color of the orange, in varying hues, is also orange, a warm, sunny shade that falls between red and yellow on the color spectrum",
                            "Mangoes are juicy stone fruit (drupe) from numerous species of tropical trees belonging to the flowering plant genus Mangifera, cultivated mostly for their edible fruit. The majority of these species are found in nature as wild mangoes"};
    int[] image={R.drawable.apple,R.drawable.banana,R.drawable.mango,R.drawable.orange};
    private String name;
    ImageView img;
    TextView tvDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        tvDetail= (TextView) findViewById(R.id.textView);
        img= (ImageView) findViewById(R.id.imageView);
        name=getIntent().getStringExtra("NAME");

        if(name.equals("Apple")){
            tvDetail.setText(detail[0]);
            img.setImageResource(image[0]);
        }
         else if(name.equals("Banana")){
            tvDetail.setText(detail[1]);
            img.setImageResource(image[1]);
        }
       else if(name.equals("Mango")){
            tvDetail.setText(detail[3]);
            img.setImageResource(image[2]);
        }
        else {
            tvDetail.setText(detail[2]);
            img.setImageResource(image[3]);
        }

    }
}
