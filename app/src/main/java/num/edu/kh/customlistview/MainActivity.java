package num.edu.kh.customlistview;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import static num.edu.kh.customlistview.R.id.image;
import static num.edu.kh.customlistview.R.id.lvFruit;

public class MainActivity extends AppCompatActivity  {
    private ListView lvfruit;
    String[] FruitName={"Apple","Banana","Mango","Orang"};
    int[] image={R.drawable.apple,R.drawable.banana,R.drawable.mango,R.drawable.orange};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvfruit= (ListView) findViewById(lvFruit);
       CustomAdapder adapder=new CustomAdapder();
        lvfruit.setAdapter(adapder);
        lvfruit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent Detail=new Intent(MainActivity.this,DetailActivity.class);
                Detail.putExtra("NAME",FruitName[i]);
                startActivity(Detail);
            }
        });
    }
    class CustomAdapder extends BaseAdapter{

        @Override
        public int getCount() {
            return image.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view=getLayoutInflater().inflate(R.layout.customize_listview,null);
            ImageView imageview=(ImageView)view.findViewById(R.id.image);
            TextView Name=(TextView)view.findViewById(R.id.tvName);

            imageview.setImageResource(image[i]);
            Name.setText(FruitName[i]);
            return view;
        }
    }
}
